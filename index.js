const fs = require("fs");
const path = require("path");
const mkdirp = require("mkdirp");
const shortid = require("shortid");

const {
  buildSchema,
  buildASTSchema,
  introspectionFromSchema,
  getIntrospectionQuery
} = require("graphql");

const resultsDirPath = path.resolve("results");
const resultsDir = mkdirp.sync(resultsDirPath);

// Read in a schame from an SDL file
const schemaDocument = fs.readFileSync("./schema/schema.graphql", "utf8");

// Build a GraphQLSchema from an SDL file
const schema = buildSchema(schemaDocument);

// Generate an introspection query
const introspectionQuery = introspectionFromSchema(schema);

// Write result
const outputFileName = `${shortid()}-output.json`;
fs.writeFileSync(
  path.resolve(resultsDirPath, outputFileName),
  JSON.stringify(introspectionQuery, null, 2)
);

console.log(`${resultsDirPath}/${outputFileName} written`);
